package tk.alex4ip.catalog.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

import org.joda.time.LocalDate;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.websystique.springmvc.dao.CategoryDao;
import com.websystique.springmvc.model.Category;

public class CategoryServiceImplTest {

	@Mock
	CategoryDao dao;
	
	@InjectMocks
	CategoryServiceImpl categoryService;
	
	@Spy
	List<Category> categories = new ArrayList<Category>();
	
	@BeforeClass
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		categories = getCategoryList();
	}

	@Test
	public void findById(){
		Category emp = categories.get(0);
		when(dao.findById(anyInt())).thenReturn(emp);
		Assert.assertEquals(categoryService.findById(emp.getId()),emp);
	}

	@Test
	public void saveCategory(){
		doNothing().when(dao).saveCategory(any(Category.class));
		categoryService.saveCategory(any(Category.class));
		verify(dao, atLeastOnce()).saveCategory(any(Category.class));
	}
	
	@Test
	public void updateCategory(){
		Category emp = categories.get(0);
		when(dao.findById(anyInt())).thenReturn(emp);
		categoryService.updateCategory(emp);
		verify(dao, atLeastOnce()).findById(anyInt());
	}

	@Test
	public void deleteCategoryBySsn(){
		doNothing().when(dao).deleteCategoryBySsn(anyString());
		categoryService.deleteCategoryBySsn(anyString());
		verify(dao, atLeastOnce()).deleteCategoryBySsn(anyString());
	}
	
	@Test
	public void findAllCategories(){
		when(dao.findAllCategories()).thenReturn(categories);
		Assert.assertEquals(categoryService.findAllCategories(), categories);
	}
	
	@Test
	public void findCategoryBySsn(){
		Category emp = categories.get(0);
		when(dao.findCategoryBySsn(anyString())).thenReturn(emp);
		Assert.assertEquals(categoryService.findCategoryBySsn(anyString()), emp);
	}

	@Test
	public void isCategorySsnUnique(){
		Category emp = categories.get(0);
		when(dao.findCategoryBySsn(anyString())).thenReturn(emp);
		Assert.assertEquals(categoryService.isCategorySsnUnique(emp.getId(), emp.getSsn()), true);
	}
	
	
	public List<Category> getCategoryList(){
		Category e1 = new Category();
		e1.setId(1);
		e1.setName("Axel");
		e1.setJoiningDate(new LocalDate());
		e1.setSalary(new BigDecimal(10000));
		e1.setSsn("XXX111");
		
		Category e2 = new Category();
		e2.setId(2);
		e2.setName("Jeremy");
		e2.setJoiningDate(new LocalDate());
		e2.setSalary(new BigDecimal(20000));
		e2.setSsn("XXX222");
		
		categories.add(e1);
		categories.add(e2);
		return categories;
	}
	
}
