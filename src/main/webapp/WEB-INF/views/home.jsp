<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Home</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script>
$(document).ready(function(){   
	$("#category").load("${pageContext.request.contextPath}/c/");
	$("#item").load("${pageContext.request.contextPath}/i/");
});
</script>
</head>
<body>

	<jsp:include flush="false" page="menu.jsp" />

	<h2>Home page ${item.name}</h2>

	<div class="row">
		<div class="col-md-6">
			<div id="category">
				cc<jsp:include flush="false" page="allcategories.jsp" />
			</div>
		</div>


		<div class="col-md-6">
			<div id="item">
				ii	<jsp:include flush="false" page="allitems.jsp" />
			</div>
		</div>
	</div>




	<jsp:include flush="false" page="footer.jsp" />
	
	
</body>
</html>
