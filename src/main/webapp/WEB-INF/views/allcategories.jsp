<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>All Category</title>

	<style>
		tr:first-child{
			font-weight: bold;
			background-color: #C6C9C4;
		}
	</style>

</head>



<body>
	<jsp:include flush="false" page="menu.jsp" />

	<h2>List of Category</h2>
	<c:if test="${!empty categories}">
		<table class="table table-striped">
			<tr>
				<td>Category</td>
				<td>Id</td>

				<c:forEach items="${categories}" var="category">
					<tr>
						<td><a href="<c:url value='/c/edit/${category.id}' />">${category.name}</a></td>
						<td><a href="<c:url value='/c/delete/${category.id}' />">delete
								#${category.id}</a></td>
					</tr>
				</c:forEach>
		</table>
	</c:if>

	<br/>
	<a class="btn btn-default" href="<c:url value='/c/new' />">Add New Category</a>
</body>
</html>