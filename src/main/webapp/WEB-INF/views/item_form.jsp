<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
   ---form---
    <table>
        <tr>
            <td><form:label path="name"><spring:message code="label.name"/></form:label></td>
        <td><form:input path="name" /></td>
        </tr>
            <td><form:label path="category"><spring:message code="label.category"/></form:label></td>
        <td>
            <form:select path="category.id">
            	<form:option value="0" label="Select" />
            	<form:options items="${categoryList}" itemValue="id" itemLabel="name" />
            </form:select>
        </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="<spring:message code="label.saveitem"/>"/>
            </td>
        </tr>
    </table>