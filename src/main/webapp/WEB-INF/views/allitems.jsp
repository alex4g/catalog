<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Items List</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<style>
		tr:first-child{
			font-weight: bold;
			background-color: #C6C9C4;
		}
	</style>
</head>
<body>
	<jsp:include flush="false" page="menu.jsp" />

	<h2>
		<spring:message code="label.items" />
	</h2>


	<c:if test="${!empty items}">
		<table class="table table-striped">

			<tr>
				<th><spring:message code="label.name" /></th>
				<th><spring:message code="label.category" /></th>
				<th>&nbsp;</th>
			</tr>
			<c:forEach items="${items}" var="item">
				<tr>
					<td><a href="<c:url value='/i/edit/${item.id}' />">${item.name}</a></td>
					<td>${item.category}</td>
					
						<td><a href="<c:url value='/i/delete/${item.id}' />"><spring:message
								code="label.delete" /> 
								 #${item.id}</a></td>
			
					<td><a
						href="${pageContext.request.contextPath}/i/edit/${item.category}"></a></td>
				</tr>
			</c:forEach>
		</table>
	</c:if>

		<br/>
	<a class="btn btn-default" href="<c:url value='/i/new' />">Add New Item</a>
	
</body>
</html>