package tk.alex4ip.catalog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tk.alex4ip.catalog.dao.ItemDAO;
import tk.alex4ip.catalog.model.Item;

@Transactional
@Service("itemService")
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemDAO itemDAO;
	
	public Item findById(int id) {
		return itemDAO.findById(id);
	}

	public void saveItem(Item item) {
		itemDAO.saveItem(item);		
	}

	public void updateItem(Item item) {
		Item entity = itemDAO.findById(item.getId());
		if(entity!=null){
			entity.setName(item.getName());
		}
	}

	public void deleteItem(int id) {
		itemDAO.deleteItem(id);		
	}

	public List<Item> findAllItems() {
		return itemDAO.findAllItems();
	}

	public List<Item> findItemsByName(String name) {
		return itemDAO.findItemsByName(name);
	}

}
