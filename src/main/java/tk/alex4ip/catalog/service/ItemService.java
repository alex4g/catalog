package tk.alex4ip.catalog.service;

import java.util.List;

import tk.alex4ip.catalog.model.Item;

public interface ItemService {

	public Item findById(int id);

	public void saveItem(Item item);

	public void updateItem(Item item);

	public void deleteItem(int id);
	
	public List<Item> findAllItems();

	public List<Item> findItemsByName(String name);

}
