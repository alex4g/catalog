package tk.alex4ip.catalog.service;

import java.util.List;

import tk.alex4ip.catalog.model.Category;

public interface CategoryService {
	
	public Category findById(int id);
	
	public void saveCategory(Category category);
	
	public void updateCategory(Category category);	
	
	public void deleteCategory(int id);
	
	public List<Category> findAllCategories();


}
