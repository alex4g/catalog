package tk.alex4ip.catalog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import tk.alex4ip.catalog.model.Category;

@NamedQueries({
	@NamedQuery(name = Item.NamedQuery.ITEM_FIND_ALL, query = "from Item"),
	@NamedQuery(name = Item.NamedQuery.ITEM_FIND_BY_ID, query = "from Item where id = :id") })
@NamedNativeQueries({ 
	@NamedNativeQuery(name = Item.NamedQuery.ITEM_FIND_BY_NAME, query = "select * from item where name like :name", resultClass = Item.class) })

@Entity
//@Table(name="items")
public class Item {
	
	@Id
	@GeneratedValue
	private int id;
	
	@Column(name = "NAME", unique = true)
//			, nullable = false)
	private String name;

	@ManyToOne
	@JoinColumn(name = "category_id")
	private Category category;
	
	
	public Item() {		
	}
	
	public Item(String name) {	
		this.name = name;
	}
	
	public Item(String name, Category category) {	
		this.name = name;
		this.category = category;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	public static class NamedQuery {
		public static final String ITEM_FIND_ALL = "Item.findAll";
		public static final String ITEM_FIND_BY_ID = "Item.findById";
		public static final String ITEM_FIND_BY_NAME = "Item.findByName";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Item))
			return false;
		Item other = (Item) obj;
		if (id != other.id)
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", name=" + name + ", category=" + category + "]";
	}
	
	
}
