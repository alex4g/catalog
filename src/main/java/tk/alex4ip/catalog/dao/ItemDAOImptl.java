package tk.alex4ip.catalog.dao;

import java.util.List;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tk.alex4ip.catalog.model.Category;
import tk.alex4ip.catalog.model.Item;
import tk.alex4ip.catalog.service.CategoryService;

@Repository("itemDAO")
public class ItemDAOImptl extends AbstractDao<Integer, Item> implements ItemDAO {

	private static final Logger logger = LoggerFactory.getLogger(ItemDAOImptl.class) ;
	
	@Autowired
	CategoryService categoryService;


	public Item findById(int id) {
		return getByKey(id);

		//  Query q = getSession().getNamedQuery( Item.NamedQuery.ITEM_FIND_BY_ID );
		//  q.setInteger("id", id);
		//  return (Item) q.uniqueResult();
	}


	public void saveItem(Item item) {		
//		int idCategory = item.getCategory().getId();
//		Category category = categoryService.findById(idCategory);
//		item.setCategory(category);
		
		logger.trace("save Item {} #{}", item.getName(), item.getId());

		Category category = categoryService.findById(item.getCategory().getId());
	
		item.setCategory(category);
		
				
		getSession().saveOrUpdate(item);	
//					persist(item);	
		
		logger.info("PAGE_OK: saved Or Updated item {} id #{} with Category {} #{}", item.getName() , item.getId(), category, category.getId());

	}

	public void deleteItem(int id) {
		delete(findById(id));

		logger.info("PAGE_OK: delete item id #{} ", id);

	}

	@SuppressWarnings("unchecked")
	public List<Item> findAllCategories() {
		return getSession().createQuery("from Item").list();

		//	  Request by criteria
		//		Criteria criteria = createEntityCriteria();
		//		return (List<Item>) criteria.list();
	}


	@SuppressWarnings("unchecked")
	public List<Item> findItemsByName(String name){
		logger.trace("find Items By Name {} #{}", name);

		Query q = getSession().getNamedQuery( Item.NamedQuery.ITEM_FIND_BY_NAME );
		q.setString("name", "%" + name + "%");
		
		logger.debug("find Items By Name {}. list ... {}", name, q);

		return q.list();
		//      return (List<Item>) q.list();

	}


	@SuppressWarnings("unchecked")
	public List<Item> findAllItems() {
		return getSession().getNamedQuery( Item.NamedQuery.ITEM_FIND_ALL ).list();
	}
}



