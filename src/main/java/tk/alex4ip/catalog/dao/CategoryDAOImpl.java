package tk.alex4ip.catalog.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import tk.alex4ip.catalog.model.Category;

@Repository("categoryDAO")
public class CategoryDAOImpl extends AbstractDao<Integer, Category> implements CategoryDAO {

	private static final Logger logger = LoggerFactory.getLogger(CategoryDAOImpl.class) ;

	public Category findById(int id) {
		return getByKey(id);
	}

	public void saveCategory(Category category) {
		
		logger.debug("save Category {} ", category);

		
//		persist(category);
		getSession().saveOrUpdate("category", category);

		logger.info("PAGE_OK: saved Or Updated Category {} id #{} ", category.getName() , category.getId());

	}
	
	public void deleteCategory(int id) {
		
		logger.trace("request delete Category {} id #{} ", findById(id).getName(), id);

		delete(findById(id));
		logger.info("PAGE_OK: delete Category id #{} ", id);

	}
	
	@SuppressWarnings("unchecked")
	public List<Category> findAllCategories() {
		
		logger.trace("create Query findAllCategories");
		
		return getSession().createQuery("from Category").list();
		
//	  Request by criteria
//		Criteria criteria = createEntityCriteria();
//		return (List<Category>) criteria.list();
	}
	
}
