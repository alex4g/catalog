package tk.alex4ip.catalog.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tk.alex4ip.catalog.model.Category;
import tk.alex4ip.catalog.model.Item;
import tk.alex4ip.catalog.service.CategoryService;
import tk.alex4ip.catalog.service.ItemService;

@Controller
//@RequestMapping("/item")
public class ItemController {

	@Autowired
	private ItemService itemService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	MessageSource messageSource;
	
	
	@RequestMapping(value = { "/i/","/items/", "/all"}, method = RequestMethod.GET)
	public String listItems(ModelMap model) {

		List<Item> items = itemService.findAllItems();
		model.addAttribute("items", items);
		return "allitems";
	}

	
	@RequestMapping(value =  "/i/new" , method = RequestMethod.GET)
	public String newItem(ModelMap model) {
		Item item = new Item();
		List<Category> categories = categoryService.findAllCategories();
		model.addAttribute("item", item);
		model.addAttribute("categories", categories) ;
		model.addAttribute("edit", false);
		return "item_edit";
	}
	
	@RequestMapping(value ={ "/i/new" , "/i/edit/*" }, method = RequestMethod.POST)
	public String saveItem(@Valid Item item, BindingResult result, 
			ModelMap model) {
		
		if (result.hasErrors()) {
			return "item_edit";
		}
//		model.addAttribute("category", categoryService.findById(item.getCategory().getId()));
		itemService.saveItem(item);
		
		return "redirect:/i/";
		}
	
	@RequestMapping("/i/delete/{itemId}")
	public String deleteItem(@PathVariable("itemId") int itemId) {

		itemService.deleteItem(itemId);

		return "redirect:/i/";
	}
	
	@RequestMapping("/i/name/{itemName}")
	public String findItems(@PathVariable("itemName") Item item, BindingResult result, ModelMap model) {

		model.addAttribute("items", itemService.findItemsByName( item.getName() ) );
		model.addAttribute("item", new Item());

		return "item_list";
	}

//
//	@RequestMapping("/item/index")
//	public String listItems(Map<String, Object> map) {
//
//		map.put("item", new Item());
//		map.put("findItem", new Item());
//		map.put("itemList", itemService.itemList());
//		map.put("categoryList", categoryService.categoryList());
//
//		return "item_list";
//	}
//
//	@RequestMapping(value = "/item/add", method = RequestMethod.POST)
//	public String addItem(@ModelAttribute("item") Item item, BindingResult result) {
//
//		itemService.addItem(item);
//
//		return "redirect:/item/index";
//	}
//

//
//	@RequestMapping("/item/save")
//	public String saveItem(@ModelAttribute("item") Item item, BindingResult result) {
//
//		itemService.addItem(item);
//
//		return "redirect:/item/edit/" + item.getId();
//	}
//
	@RequestMapping("/i/edit/{itemId}")
	public String editItem(@PathVariable("itemId") int itemId, ModelMap map) {

		Item item = itemService.findById(itemId);
		map.addAttribute("item", item); 
		map.addAttribute("categories", categoryService.findAllCategories());

//		return "redirect:/i/";
		return "item_edit";
	}
//
//	@RequestMapping("/item/find")
//	public String findItems(@ModelAttribute("findItem") Item item, BindingResult result, Map<String, Object> map) {
//
//		map.put("itemList", itemService.findItemsByName( item.getName() ) );
//		map.put("item", new Item());
//
//		return "item_list";
//	}

}
