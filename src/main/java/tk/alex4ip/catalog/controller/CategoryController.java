package tk.alex4ip.catalog.controller;

import javax.validation.Valid;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.springframework.validation.FieldError;


import tk.alex4ip.catalog.model.Category;
import tk.alex4ip.catalog.service.CategoryService;

@Controller
//@RequestMapping("/c/")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;
    	
	@Autowired
	MessageSource messageSource;
  
	@RequestMapping(value = { "/categories/", "/c/" }, method = RequestMethod.GET)
	public String listCategories(ModelMap model) {

		List<Category> categories = categoryService.findAllCategories();
		model.addAttribute("categories", categories);
		return "allcategories";
	}
	
	@RequestMapping(value =  "/c/new" , method = RequestMethod.GET)
	public String newCategory(ModelMap model) {
		Category category = new Category();
		model.addAttribute("category", category);
		model.addAttribute("edit", false);
		return "category_edit";
	}
	
	@RequestMapping(value ={ "/c/new", "/c/edit/*"}, method = RequestMethod.POST)
	public String addCategory(@Valid Category category, BindingResult result, 
			ModelMap model) {
		
		if (result.hasErrors()) {
			return "category_edit";
		}
		
		categoryService.saveCategory(category);
		
//		model.addAttribute("success", "Category " + category.getName() + " registered successfully");
		return "redirect:/c/";
		}

	@RequestMapping(value = { "/c/delete/{categoryId}" }, method = RequestMethod.GET)
	public String deleteCategory(@PathVariable int categoryId, ModelMap model) {
		categoryService.deleteCategory(categoryId);
		return "redirect:/c/";
	}
	
    @RequestMapping("/c/{categoryId}")
    public String showCategoryId(@PathVariable("categoryId") int categoryId, ModelMap model) {
		Category category = categoryService.findById(categoryId);
		model.addAttribute("category", category);
		
		return "allcategories";
    }
	

		@RequestMapping(value = { "/c/edit/{categoryId}" }, method = RequestMethod.GET)
		public ModelAndView editCategory(@PathVariable int categoryId, ModelAndView model) {
			
//			categoryService.findById(categoryId);
			model.setViewName( "category_edit" );
			model.addObject(categoryService.findById(categoryId));
			model.addObject("edit", true);
//			addAttribute("edit", false);

			return model;

		}

}
    


